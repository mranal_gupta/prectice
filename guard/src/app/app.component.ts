import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'guard';
  demo: string = 'Interpolation binding Example';
  demotwo: string = 'Property binding Example';
  cols:number = 3;
  bdr:number = 5;
  data:string = 'chandan';
  data1:string = ' Rahul';
  constructor(){ }

  people:any[]=[
    {
      "name":"Kumar",
      "Country":'India'
    },
    {
      "name":"Chandan",
      "Country":'UK'
    },
    {
      "name":"Savle",
      "Country":'UK'
    },
    {
      "name":"popo",
      "Country":'India'
    },
    {
      "name":"silavaaaaa",
      "Country":'UK'
    },
    {
      "name":"Kumar",
      "Country":'UK'
    },
    {
      "name":"Jmura",
      "Country":'India'
    },
  ];

  getcolor(Country) {
    switch(Country){
      case 'India':
        return 'green';
        case 'UK':
        return 'blue';
    }
  }
    showdata(){
      alert("Event Binding is working");

    }

  }
