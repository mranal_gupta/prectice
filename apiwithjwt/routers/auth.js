//MiddleWare File
const router = require("express").Router();
const User = require("../model/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { registerValidation, loginValidation } = require("../validation");

//For Registration Page
router.post("/register", async (req, res) => {

  //Check Users Validation
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  //Check user exist or not
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist) return res.status(400).send("Email Already Exists");

  //Hash Passwords
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword
  });

  try {
    const savedUser = await user.save();
    res.send(savedUser);
  } catch (err) {
    res.status(400).send(err);
  }
});

//For login Page
router.post("/login", async (req, res) => {
  //Check Users Validation
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  console.log(error);

  //Check user exist or not
  const users = await User.findOne({ email: req.body.email });
  if (!users) return res.status(400).send("Email is not found");

  //It will check password is valid or not.
  const validPass = await bcrypt.compare(req.body.password, users.password);
  if (!validPass) return res.status(400).send("Invalid Password");

  //For JWT Token With Header
  const token = jwt.sign({ _id: users._id }, process.env.TOKEN_SECRET);
  res.header("auth-token", token).send(token);

  // res.send("Logged in!");
});

module.exports = router;
