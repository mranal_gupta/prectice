import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DetailsComponent } from "./details/details.component";
import { DetailsRoutingModule } from "./details-routing.module";

@NgModule({
  declarations: [DetailsComponent],
  imports: [CommonModule, DetailsRoutingModule],
  exports: [DetailsComponent],
  bootstrap: [DetailsComponent]
})
export class DetailsModule {
  constructor() {
    console.log("Details module are working");
  }
}
