import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-testfour",
  templateUrl: "./testfour.component.html",
  styleUrls: ["./testfour.component.css"]
})
export class TestfourComponent implements OnInit {
  userName: string = "";

  constructor(private _designeservices: AppService) {
    this._designeservices.userName.subscribe(uname => {
      this.userName = uname;
    });
  }

  ngOnInit() {}
  updateUserName(uname) {
    this._designeservices.userName.next(uname.value);
  }
}
