import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutertwoComponent } from './routertwo.component';

describe('RoutertwoComponent', () => {
  let component: RoutertwoComponent;
  let fixture: ComponentFixture<RoutertwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutertwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutertwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
