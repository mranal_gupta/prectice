import { Component, OnInit } from "@angular/core";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-routertwo",
  templateUrl: "./routertwo.component.html",
  styleUrls: ["./routertwo.component.css"]
})
export class RoutertwoComponent implements OnInit {
  constructor(private cookie: CookieService) {
    console.log(cookie, "Cookies");
  }

  ngOnInit() {}

  click() {
    console.log("hellow");
    this.cookie.set("userID", "ABC");
    this.cookie.set("userID2", "dABC");
    this.cookie.set("userI3", "sABC");
    alert(this.cookie.get("userID"));
  }
}
