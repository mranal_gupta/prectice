import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams
} from "@angular/common/http";
import { Observable, throwError, Subject, BehaviorSubject } from "rxjs";

interface cars {
  body: any;
}

@Injectable({
  providedIn: "root"
})
export class AppService {
  constructor(private http: HttpClient) {}

  getAllPosts(): Observable<cars> {
    // const headers = new HttpHeaders ({
    //   'Name':'Mranal'
    // });
    // const params = new HttpParams().append('age','100');
    return this.http.get<cars>("https://jsonplaceholder.typicode.com/posts");
  }

  // userName = new Subject<any>();
  userName = new BehaviorSubject("Hellow Dude");
}
