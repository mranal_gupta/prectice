import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { MyserviceService } from "../myservice.service";

@Component({
  selector: "app-student",
  templateUrl: "./student.component.html",
  styleUrls: ["./student.component.css"]
})
export class StudentComponent implements OnInit {
  title: string;

  deptobj: any[];
  constructor(private _service: MyserviceService) {}

  //Use of input decorator
  @Input() myinput: string;
  @Output() myoutput: EventEmitter<string> = new EventEmitter();
  outputstring = "Hii i am your child";

  ngOnInit() {
    this.deptobj = this._service.dept;
    console.log(this.myinput);
  }
  senddata() {
    this.myoutput.emit(this.outputstring);
  }

  click() {
    console.log("clicked");
  }
}
