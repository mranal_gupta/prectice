import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppInterceptorService } from "./app-interceptor.service";
import { StudentComponent } from "./student/student.component";
import { CommonModule } from "@angular/common";
import { CounterComponent } from "./counter/counter.component";
import { CounterParentComponent } from "./counter-parent/counter-parent.component";
import { RouteroneComponent } from "./routerone/routerone.component";
import { RoutertwoComponent } from "./routertwo/routertwo.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";
import { ChildComponent } from "./child/child.component";
import { CookieService } from "ngx-cookie-service";
import { TestoneComponent } from "./testone/testone.component";
import { TesttwoComponent } from "./testtwo/testtwo.component";
import { TestthreeComponent } from "./testthree/testthree.component";
import { TestfourComponent } from "./testfour/testfour.component";
import { AdsComponent } from "./ads/ads.component";

@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    CounterComponent,
    CounterParentComponent,
    RouteroneComponent,
    RoutertwoComponent,
    PagenotfoundComponent,
    ChildComponent,
    TestoneComponent,
    TesttwoComponent,
    TestthreeComponent,
    TestfourComponent,
    AdsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    ChartsModule
  ],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
