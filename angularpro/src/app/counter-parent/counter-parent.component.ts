import { Component, OnInit, ViewChild } from "@angular/core";
import { CounterComponent } from "../counter/counter.component";
import { ChartDataSets, ChartOptions } from "chart.js";
import { Label, Color } from "ng2-charts";
@Component({
  selector: "app-counter-parent",
  templateUrl: "./counter-parent.component.html",
  styleUrls: ["./counter-parent.component.css"]
})
export class CounterParentComponent {
  @ViewChild(CounterComponent, { static: true })
  counterComponent: CounterComponent;

  increase() {
    this.counterComponent.increasebyone();
  }
  decrease() {
    this.counterComponent.decreasebyone();
  }
  constructor() {}

  //Pie Chart
  public pieChartLabels: string[] = [
    "Chrome",
    "Safari",
    "Firefox",
    "Internet Explorer",
    "Other"
  ];
  public pieChartData: number[] = [40, 20, 20, 10, 10];
  public pieChartType: string = "pie";

  public chartClicked(e: any): void {
    console.log(e, "click");
  }
  public chartHovered(e: any): void {
    console.log(e, "hover");
  }

  //Line Chart
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: "Series A" }
  ];

  public lineChartLabels: Label[] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July"
  ];
  public linerChartColors: Color[] = [
    {
      borderColor: "black",
      backgroundColor: "rgba(255,0,0,0.3)"
    }
  ];
  public linerChartLegend = true;
  public linerChartType = "line";
  public linerChartPlugins = [];
}
