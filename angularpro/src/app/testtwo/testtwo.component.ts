import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-testtwo",
  templateUrl: "./testtwo.component.html",
  styleUrls: ["./testtwo.component.css"]
})
export class TesttwoComponent implements OnInit {
  userName: string = "";
  constructor(private _designeservices: AppService) {
    this._designeservices.userName.subscribe(uname => {
      this.userName = uname;
    });
  }

  ngOnInit() {}

  updateUserName(uname) {
    console.log(this._designeservices);
    this._designeservices.userName.next(uname.value);
  }
}
