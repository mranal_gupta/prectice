import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RouteroneComponent } from "./routerone/routerone.component";
import { RoutertwoComponent } from "./routertwo/routertwo.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";
import { ChildComponent } from "./child/child.component";
import { AdsComponent } from "./ads/ads.component";

const routes: Routes = [
  {
    path: "routerone",
    children: [
      { path: "", component: RouteroneComponent },
      {
        path: "routertwo",
        component: RoutertwoComponent
      },
      {
        path: "child",
        component: ChildComponent
      }
    ]
  },
  {
    path: "ads",  
    component: AdsComponent
  },

  {
    path: "**",
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
