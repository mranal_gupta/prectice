import { Component, OnInit, ViewChild } from "@angular/core";

class MyProvider {
  getstring(arg0: string) {
    throw new Error("Method not implemented.");
  }
  constructor() {
    console.log("view provider runnning");
  }
  varMyProvider = "varmyprovider";
}

class MyProvider1 {
  constructor() {
    console.log("Var my profile is running");
  }
  varmyprovider1 = "var my provider1";
  getstring(name) {
    console.log("varmyprodiver1" + name);
  }
}

@Component({
  selector: "app-child",
  templateUrl: "./child.component.html",
  styleUrls: ["./child.component.css"],
  viewProviders: [MyProvider1, MyProvider]
})
export class ChildComponent implements OnInit {

  constructor(public obj1: MyProvider, public obj2: MyProvider1) {
    
    obj2.getstring("");

    console.log(obj1.varMyProvider);

    console.log(obj2.varmyprovider1);
  }

  title = "app";

  @ViewChild("cdm", { static: true }) element;

  ngOnInit() {
    console.log(this.element, "Result");
  }
}
