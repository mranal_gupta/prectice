import { Component, Inject } from "@angular/core";
import { AppService } from "./app.service";
import { DemoService } from "./demo.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  inputvariable: string = "Hiii I am Your Parent";
  model: any = {};
  data: string;
  data123:string="this Mranal";

  //Injectable Decorator
  constructor(
    private demo: DemoService,
    private appservice: AppService,
    @Inject(AppService) appService
  ) {
    console.log(appService);
    console.log("Hi.. This is component");
  }

  ngOnInit() {
    this.appservice.getAllPosts().subscribe(
      data => {
        console.log(data[0].body);
      },
      error => {
        console.log("error occured", error);
      }
    );
    this.data = this.demo.display();
    console.log(this.data, "sasasasa");
  }

  //Hostlistener Decorator
  onSubmit() {
    console.log(this.model);
  }
  getdata(value) {
    console.log(value);
  }
}
