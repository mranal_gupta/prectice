import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-testone",
  templateUrl: "./testone.component.html",
  styleUrls: ["./testone.component.css"]
})
export class TestoneComponent implements OnInit {
  userName: string = "";
  constructor(private _designeservices: AppService) {
    this._designeservices.userName.subscribe(uname => {
      this.userName = uname;
    });
  }

  ngOnInit() {}

  updateUserName(uname) {
    this._designeservices.userName.next(uname.value);
  }
}
