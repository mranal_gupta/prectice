import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MyserviceService {
  title: string;

  dept: any[];

  constructor() {
    this.title = "Solution";

    this.dept = [
      { id: 1, name: "Account" },
      { id: 2, name: "Admin" },
      { id: 3, name: "Staff" }
    ];
  }

  display() {}  
}
