import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteroneComponent } from './routerone.component';

describe('RouteroneComponent', () => {
  let component: RouteroneComponent;
  let fixture: ComponentFixture<RouteroneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteroneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteroneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
