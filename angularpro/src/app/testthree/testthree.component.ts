import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-testthree",
  templateUrl: "./testthree.component.html",
  styleUrls: ["./testthree.component.css"]
})
export class TestthreeComponent implements OnInit {
  userName: string = "";
  constructor(private _designeservices: AppService) {
    this._designeservices.userName.subscribe(uname => {
      this.userName = uname;
    });
  }

  ngOnInit() {}

  updateUserName(uname) {
    this._designeservices.userName.next(uname.value);
  }

  

}
