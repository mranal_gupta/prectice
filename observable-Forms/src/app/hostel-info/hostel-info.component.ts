import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder, NgForm } from "@angular/forms";

@Component({
  selector: "app-hostel-info",
  templateUrl: "./hostel-info.component.html",
  styleUrls: ["./hostel-info.component.css"]
})
export class HostelInfoComponent implements OnInit {
  form: FormGroup;
  email: string = "";
  password: string = "";

  constructor(private frmbuilder: FormBuilder) {
    
    console.log(frmbuilder);
    this.form = frmbuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.maxLength(5)]]
    });
  }

  ngOnInit() {}

  submit(form: NgForm) {
    console.log(form.controls);
  }
}
