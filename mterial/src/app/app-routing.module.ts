import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MaterialtableComponent } from "./materialtable/materialtable.component";

const routes: Routes = [
  {
    path: "table",
    component: MaterialtableComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
