import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator, MatTableDataSource } from "@angular/material";

@Component({
  selector: "app-materialtable",
  templateUrl: "./materialtable.component.html",
  styleUrls: ["./materialtable.component.scss"]
})
export class MaterialtableComponent implements OnInit {
  constructor() {}

  displayedColumns: string[] = ["position", "name", "weight", "symbol"];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: "Hydorgen", weight: 1.256, symbol: "H" },
  { position: 2, name: "Helium", weight: 3.256, symbol: "He" },
  { position: 3, name: "Berylium", weight: 4.256, symbol: "Li" },
  { position: 4, name: "Boron", weight: 9.256, symbol: "Be" },
  { position: 5, name: "Nitrogen", weight: 5.256, symbol: "C" },
  { position: 6, name: "carbon", weight: 4.256, symbol: "N" },
  { position: 7, name: "Flurine", weight: 60.256, symbol: "O" },
  { position: 8, name: "Neon", weight: 5.56, symbol: "F" },
  { position: 9, name: "Hydorgen", weight: 2.56, symbol: "Ne" },
  { position: 10, name: "Hydorgen", weight: 0.256, symbol: "V" },
  { position: 11, name: "Hydorgen", weight: 8.256, symbol: "Br" },
  { position: 12, name: "Hydorgen", weight: 7.256, symbol: "Hi" }
];
