import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatButtonModule,
  MatBadgeModule,
  MatTooltipModule,
  MatTableModule,
  MatPaginatorModule
} from "@angular/material";

const Materialcomponents = [
  MatButtonModule,
  MatBadgeModule,
  MatTableModule,
  MatTooltipModule,
  MatPaginatorModule
];

@NgModule({
  exports: [Materialcomponents],
  imports: [CommonModule, Materialcomponents]
})
export class MaterialModule {}
