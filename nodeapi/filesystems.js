const fs = require("fs");

//with http calls
// const http = require("http");
// const server = http.createServer(function(req, res) {
//     fs.readFile(__dirname + "/hello.txt", "utf8", (err, data) => {
//       res.writeHead(200, { "content-type": "text/plain" });
//       res.write(data);
//       res.end();
//     });
//   })
//   .listen(4000, () => console.log("Server Runnning on port 3000"));

// fs.unlink(__dirname+"/hello.txt",(err,data)=>{
//     if(err) throw err;
// console.log("File Deleted"+data);
// });

try {

    const data = fs.readFileSync(__dirname+"/hello.txt","utf8");
    console.log(data);
} catch(e){

    console.log(e);
}

console.log("File Ended Successfully");