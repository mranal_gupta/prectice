//With Out CallBackHell

var promise = new Promise(function(resolve,reject){
    resolve(5);
})

promise.then(addition).then(substract).then(multiplication).then(function(msg){
console.log('Result '+msg);
}).catch(function(err) {
    console.log("Errot "+err);
});


function addition(val){
    return val+5;
}
function substract(val){
    return val-3;
}
function multiplication(val){
    return val*5;
}


// BY CallBack Hell
// addition(5,function(addRes,err){
//     if(!err){
//         substract(addRes,function(subRes,err){
//             if(!err){
//                 multiplication(subRes,function(multRes,err){
//                     if(!err) {
//                         console.log('Result'+multRes);
//                     }
//                 });
//             }
//         });
//     }
// });

// function addition(val,callback){
//     return callback(val+5,false);
// }
// function substract(val,callback){
//     return callback(val-3,false);
// }
// function multiplication(val,callback){
//     return callback(val*5,false);
// }



