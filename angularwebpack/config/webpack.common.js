var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var helper = require("./helpers");

module.exports = {
  entry: {
    polyfills: "./src/polyfills.ts",
    vendor: "./src/vendor.ts",
    app: "./src/main.ts"
  },

  resolve: {
    extensions: [".ts", ".js"]
  },

  module: {
    rules: [
      {
        test: /\.ts/,
        loaders: [
          {
            loader: "awesome-typescript-loader",
            options: { configFileName: helper.root("src", "tsconfig.json") }
          },
          "angular2-template-loader"
        ]
      },

      {
        test: /\.html$/,
        loader: "html-loader"
      },

      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: "file-loader?name=assets/[name].[hash].[ext]"
      },

      {
        test: /\.css$/,
        exclude: helper.root("src", "app"),
        loader: ExtractTextPlugin.extract({
          fallbackLoader: "style-loader",
          loader: "css-loader?sourceMap"
        })
      },

      {
        test: /\.css$/,
        include: helper.root("src", "app"),
        loader: "raw-loader"
      }
      
    ]
  },

  plugins: [
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/) core(\\|\/)fesm5/,
      helper.root("./src"),
      {}
    ),

    new webpack.optimize.CommonsChunkPlugin({
      name: ["app", "vendor", "ployfills"]
    }),

   new HtmlWebpackPlugin({
      template: "src/index.html"
    })

  ]
};
