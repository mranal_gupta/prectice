var webpackConfig = require("./webpack.test");
require("karma-jasmine-html-reporter");

module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine", "@angular/cli"],
    plugins: [
      require("./karma-test-shim"),
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require("karma-jasmine-html-reporter"),
      require("karma-coverage-istanbul-reporter"),
      require("@angular/cli/plugins/karma")
    ],

    files: [{ pattern: "./config/karma-test-shim.js", watched: false }],

    perprocessors: {
      "./config/karma-test-shim.js": ["webpack", "sourcemap "]
    },
    webpack: webpackConfig,

    webpackMiddleware: {
      stats: "errors-only"
    },

    webpackServer: {
      noInfo: true
    },
    angularCli: {
      environment: "dev"
    },

    reporters:
      config.angularCli && config.angularCli.codeCoverage
        ? ["progress", "karma-remap-istanbul", "kjhtml"]
        : ["progress", "kjhtml"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    singleRun: true
  });
};
