const MomentLocalesPlugin = require("moment-locales-webpack-plugin");

module.exports = {
  plugin: [
    new MomentLocalesPlugin({
      localesToKeep: ["fr"]
    })
  ]
};
