import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { FormControl } from "@angular/forms";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  
  title = "observableVSpromise";
  private results = [];
  constructor(private http: HttpClient) {}
  private term = new FormControl();

  ngOnInit() {
    console.log(this.term);
    this.term.valueChanges.subscribe(searchTerm => {
      this.http
        .get(`https://restcountries.eu/rest/v2/name/${searchTerm}`)
        .subscribe((data: any) => {
          console.time("request-length");
          console.log(data);
          console.timeEnd("request-length");
          this.results = data;
        });
    });
  }
}

// private search(term:any){
//   console.log(term);
//   this.http.get(`https://restcountries.eu/rest/v2/name/${term}`).toPromise()
//   .then((data: any) => {
//     console.time('request-length');
//     console.log(data);
//     console.timeEnd('request-length');
//     this.results = data;
//   });
// }
