import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PortComponent } from "./port/port.component";
import { PortRoutingModule } from "./port-routing.module";

@NgModule({
  declarations: [PortComponent],
  imports: [CommonModule, PortRoutingModule],
  exports: [PortComponent]
})
export class PortModule {}
