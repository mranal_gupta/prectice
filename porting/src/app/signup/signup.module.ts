import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SignupComponent } from "./signup/signup.component";
import { SignupRoutingModule } from "./signup-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NavbarComponent } from "../reusable/navbar/navbar.component";

@NgModule({
  declarations: [SignupComponent, NavbarComponent],
  imports: [
    CommonModule,
    SignupRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [SignupComponent, NavbarComponent]
})
export class SignupModule {}
