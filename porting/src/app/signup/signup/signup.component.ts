import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup, NgForm } from "@angular/forms";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit {
  forms: FormGroup;
  fname: string = "";
  lname: String = "";
  email: string = "";
  phone: string = "";

  constructor(formbuilders: FormBuilder) {
    this.forms = formbuilders.group({
      fname: ["", Validators.required],
      lname: ["", Validators.required],
      email: ["", Validators.required],
      phone: ["", Validators.required]
    });
  }

  signup(forms: NgForm) {
    console.log(forms.value, "Forms Change");
  }

  ngOnInit() {}
}
