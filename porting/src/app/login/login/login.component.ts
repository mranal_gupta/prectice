import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  forms: FormGroup;
  email: string = "";
  password: string = "";

  constructor(formsbuilder: FormBuilder) {
    console.log(formsbuilder, "Builder");

    this.forms = formsbuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  ngOnInit() {}

  login(forms: NgForm) {
    console.log(forms, "Formsassssssssssssssssssssssssss");
  }
}
