import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ErrorComponent } from "./error/error.component";

const routes: Routes = [
  {
    path: "",
    loadChildren: "src/app/home/home.module#HomeModule"
  },
  {
    path: "login",
    loadChildren: "src/app/login/login.module#LoginModule"
  },
  {
    path: "signup",
    loadChildren: "src/app/signup/signup.module#SignupModule"
  },
  {
    path: "port",
    loadChildren: "src/app/port/port.module#PortModule"
  },
  {
    path: "**",
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
