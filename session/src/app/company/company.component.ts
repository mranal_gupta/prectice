import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-company",
  templateUrl: "./company.component.html",
  styleUrls: ["./company.component.css"]
})
export class CompanyComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    
    let obj = { fname: "Chandan", lname: "Singh" };
    localStorage.setItem("userid", JSON.stringify(obj));
    localStorage.setItem("localData", "Local-Chandan");
    sessionStorage.setItem("seesionData", "Seesion-Ajeet");
    sessionStorage.clear();
    
    console.log(JSON.parse(localStorage.getItem("userid")));
    console.log(localStorage.getItem("localData"));
    console.log(sessionStorage.getItem("seesionData"));
    if (window.localStorage) {
      alert("supported");
    } else {
      alert("Not Supported  ");
    }
  }
}
